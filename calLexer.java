// Generated from cal.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class calLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		VARIABLE=1, CONSTANT=2, RETURN=3, INTEGER=4, BOOLEAN=5, VOID=6, MAIN=7, 
		IF=8, ELSE=9, TRUE=10, FALSE=11, WHILE=12, BEGIN=13, END=14, IS=15, SKIP_=16, 
		ASSIGN=17, COMMA=18, SEMI_COLON=19, COLON=20, LBR=21, RBR=22, PLUS=23, 
		MINUS=24, NEG=25, OR=26, AND=27, EQUAL=28, NOT_EQUAL=29, LESS_THAN=30, 
		LESS_THAN_EQ=31, MORE_THAN=32, MORE_THAN_EQ=33, IDENTIFIER=34, NUMBER=35, 
		COMMENT=36, BLOCKCOMMENT=37, WS=38;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"V", "A", "R", "I", "B", "L", "E", "C", "O", "N", "S", "T", "U", "G", 
		"D", "M", "F", "W", "H", "K", "P", "LETTER", "DIGGIT", "UNDERSCORE", "VARIABLE", 
		"CONSTANT", "RETURN", "INTEGER", "BOOLEAN", "VOID", "MAIN", "IF", "ELSE", 
		"TRUE", "FALSE", "WHILE", "BEGIN", "END", "IS", "SKIP_", "ASSIGN", "COMMA", 
		"SEMI_COLON", "COLON", "LBR", "RBR", "PLUS", "MINUS", "NEG", "OR", "AND", 
		"EQUAL", "NOT_EQUAL", "LESS_THAN", "LESS_THAN_EQ", "MORE_THAN", "MORE_THAN_EQ", 
		"IDENTIFIER", "NUMBER", "COMMENT", "BLOCKCOMMENT", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "':='", "','", "';'", "':'", "'('", "')'", 
		"'+'", "'-'", "'~'", "'|'", "'&'", "'='", "'!='", "'<'", "'<='", "'>'", 
		"'>='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "VARIABLE", "CONSTANT", "RETURN", "INTEGER", "BOOLEAN", "VOID", 
		"MAIN", "IF", "ELSE", "TRUE", "FALSE", "WHILE", "BEGIN", "END", "IS", 
		"SKIP_", "ASSIGN", "COMMA", "SEMI_COLON", "COLON", "LBR", "RBR", "PLUS", 
		"MINUS", "NEG", "OR", "AND", "EQUAL", "NOT_EQUAL", "LESS_THAN", "LESS_THAN_EQ", 
		"MORE_THAN", "MORE_THAN_EQ", "IDENTIFIER", "NUMBER", "COMMENT", "BLOCKCOMMENT", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public calLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "cal.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2(\u016d\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3"+
		"\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20"+
		"\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27"+
		"\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36"+
		"\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3!\3!\3!\3\"\3"+
		"\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3%\3%\3%\3%\3%\3%\3&\3"+
		"&\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3(\3(\3(\3)\3)\3)\3)\3)\3*\3*\3*\3+\3+\3"+
		",\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3"+
		"\64\3\65\3\65\3\66\3\66\3\66\3\67\3\67\38\38\38\39\39\3:\3:\3:\3;\3;\3"+
		";\3;\7;\u0138\n;\f;\16;\u013b\13;\3<\7<\u013e\n<\f<\16<\u0141\13<\3<\3"+
		"<\7<\u0145\n<\f<\16<\u0148\13<\3<\5<\u014b\n<\3=\3=\3=\3=\7=\u0151\n="+
		"\f=\16=\u0154\13=\3=\3=\3>\3>\3>\3>\3>\7>\u015d\n>\f>\16>\u0160\13>\3"+
		">\3>\3>\3>\3>\3?\6?\u0168\n?\r?\16?\u0169\3?\3?\3\u015e\2@\3\2\5\2\7\2"+
		"\t\2\13\2\r\2\17\2\21\2\23\2\25\2\27\2\31\2\33\2\35\2\37\2!\2#\2%\2\'"+
		"\2)\2+\2-\2/\2\61\2\63\3\65\4\67\59\6;\7=\b?\tA\nC\13E\fG\rI\16K\17M\20"+
		"O\21Q\22S\23U\24W\25Y\26[\27]\30_\31a\32c\33e\34g\35i\36k\37m o!q\"s#"+
		"u$w%y&{\'}(\3\2\34\4\2XXxx\4\2CCcc\4\2TTtt\4\2KKkk\4\2DDdd\4\2NNnn\4\2"+
		"GGgg\4\2EEee\4\2QQqq\4\2PPpp\4\2UUuu\4\2VVvv\4\2WWww\4\2IIii\4\2FFff\4"+
		"\2OOoo\4\2HHhh\4\2YYyy\4\2JJjj\4\2MMmm\4\2RRrr\4\2C\\c|\3\2\62;\3\2\63"+
		";\4\2\f\f\17\17\5\2\13\f\17\17\"\"\2\u015e\2\63\3\2\2\2\2\65\3\2\2\2\2"+
		"\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C"+
		"\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2"+
		"\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2"+
		"\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i"+
		"\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2"+
		"\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\3\177\3\2\2\2\5\u0081"+
		"\3\2\2\2\7\u0083\3\2\2\2\t\u0085\3\2\2\2\13\u0087\3\2\2\2\r\u0089\3\2"+
		"\2\2\17\u008b\3\2\2\2\21\u008d\3\2\2\2\23\u008f\3\2\2\2\25\u0091\3\2\2"+
		"\2\27\u0093\3\2\2\2\31\u0095\3\2\2\2\33\u0097\3\2\2\2\35\u0099\3\2\2\2"+
		"\37\u009b\3\2\2\2!\u009d\3\2\2\2#\u009f\3\2\2\2%\u00a1\3\2\2\2\'\u00a3"+
		"\3\2\2\2)\u00a5\3\2\2\2+\u00a7\3\2\2\2-\u00a9\3\2\2\2/\u00ab\3\2\2\2\61"+
		"\u00ad\3\2\2\2\63\u00af\3\2\2\2\65\u00b8\3\2\2\2\67\u00c1\3\2\2\29\u00c8"+
		"\3\2\2\2;\u00d0\3\2\2\2=\u00d8\3\2\2\2?\u00dd\3\2\2\2A\u00e2\3\2\2\2C"+
		"\u00e5\3\2\2\2E\u00ea\3\2\2\2G\u00ef\3\2\2\2I\u00f5\3\2\2\2K\u00fb\3\2"+
		"\2\2M\u0101\3\2\2\2O\u0105\3\2\2\2Q\u0108\3\2\2\2S\u010d\3\2\2\2U\u0110"+
		"\3\2\2\2W\u0112\3\2\2\2Y\u0114\3\2\2\2[\u0116\3\2\2\2]\u0118\3\2\2\2_"+
		"\u011a\3\2\2\2a\u011c\3\2\2\2c\u011e\3\2\2\2e\u0120\3\2\2\2g\u0122\3\2"+
		"\2\2i\u0124\3\2\2\2k\u0126\3\2\2\2m\u0129\3\2\2\2o\u012b\3\2\2\2q\u012e"+
		"\3\2\2\2s\u0130\3\2\2\2u\u0133\3\2\2\2w\u014a\3\2\2\2y\u014c\3\2\2\2{"+
		"\u0157\3\2\2\2}\u0167\3\2\2\2\177\u0080\t\2\2\2\u0080\4\3\2\2\2\u0081"+
		"\u0082\t\3\2\2\u0082\6\3\2\2\2\u0083\u0084\t\4\2\2\u0084\b\3\2\2\2\u0085"+
		"\u0086\t\5\2\2\u0086\n\3\2\2\2\u0087\u0088\t\6\2\2\u0088\f\3\2\2\2\u0089"+
		"\u008a\t\7\2\2\u008a\16\3\2\2\2\u008b\u008c\t\b\2\2\u008c\20\3\2\2\2\u008d"+
		"\u008e\t\t\2\2\u008e\22\3\2\2\2\u008f\u0090\t\n\2\2\u0090\24\3\2\2\2\u0091"+
		"\u0092\t\13\2\2\u0092\26\3\2\2\2\u0093\u0094\t\f\2\2\u0094\30\3\2\2\2"+
		"\u0095\u0096\t\r\2\2\u0096\32\3\2\2\2\u0097\u0098\t\16\2\2\u0098\34\3"+
		"\2\2\2\u0099\u009a\t\17\2\2\u009a\36\3\2\2\2\u009b\u009c\t\20\2\2\u009c"+
		" \3\2\2\2\u009d\u009e\t\21\2\2\u009e\"\3\2\2\2\u009f\u00a0\t\22\2\2\u00a0"+
		"$\3\2\2\2\u00a1\u00a2\t\23\2\2\u00a2&\3\2\2\2\u00a3\u00a4\t\24\2\2\u00a4"+
		"(\3\2\2\2\u00a5\u00a6\t\25\2\2\u00a6*\3\2\2\2\u00a7\u00a8\t\26\2\2\u00a8"+
		",\3\2\2\2\u00a9\u00aa\t\27\2\2\u00aa.\3\2\2\2\u00ab\u00ac\t\30\2\2\u00ac"+
		"\60\3\2\2\2\u00ad\u00ae\7a\2\2\u00ae\62\3\2\2\2\u00af\u00b0\5\3\2\2\u00b0"+
		"\u00b1\5\5\3\2\u00b1\u00b2\5\7\4\2\u00b2\u00b3\5\t\5\2\u00b3\u00b4\5\5"+
		"\3\2\u00b4\u00b5\5\13\6\2\u00b5\u00b6\5\r\7\2\u00b6\u00b7\5\17\b\2\u00b7"+
		"\64\3\2\2\2\u00b8\u00b9\5\21\t\2\u00b9\u00ba\5\23\n\2\u00ba\u00bb\5\25"+
		"\13\2\u00bb\u00bc\5\27\f\2\u00bc\u00bd\5\31\r\2\u00bd\u00be\5\5\3\2\u00be"+
		"\u00bf\5\25\13\2\u00bf\u00c0\5\31\r\2\u00c0\66\3\2\2\2\u00c1\u00c2\5\7"+
		"\4\2\u00c2\u00c3\5\17\b\2\u00c3\u00c4\5\31\r\2\u00c4\u00c5\5\33\16\2\u00c5"+
		"\u00c6\5\7\4\2\u00c6\u00c7\5\25\13\2\u00c78\3\2\2\2\u00c8\u00c9\5\t\5"+
		"\2\u00c9\u00ca\5\25\13\2\u00ca\u00cb\5\31\r\2\u00cb\u00cc\5\17\b\2\u00cc"+
		"\u00cd\5\35\17\2\u00cd\u00ce\5\17\b\2\u00ce\u00cf\5\7\4\2\u00cf:\3\2\2"+
		"\2\u00d0\u00d1\5\13\6\2\u00d1\u00d2\5\23\n\2\u00d2\u00d3\5\23\n\2\u00d3"+
		"\u00d4\5\r\7\2\u00d4\u00d5\5\17\b\2\u00d5\u00d6\5\5\3\2\u00d6\u00d7\5"+
		"\25\13\2\u00d7<\3\2\2\2\u00d8\u00d9\5\3\2\2\u00d9\u00da\5\23\n\2\u00da"+
		"\u00db\5\t\5\2\u00db\u00dc\5\37\20\2\u00dc>\3\2\2\2\u00dd\u00de\5!\21"+
		"\2\u00de\u00df\5\5\3\2\u00df\u00e0\5\t\5\2\u00e0\u00e1\5\25\13\2\u00e1"+
		"@\3\2\2\2\u00e2\u00e3\5\t\5\2\u00e3\u00e4\5#\22\2\u00e4B\3\2\2\2\u00e5"+
		"\u00e6\5\17\b\2\u00e6\u00e7\5\r\7\2\u00e7\u00e8\5\27\f\2\u00e8\u00e9\5"+
		"\17\b\2\u00e9D\3\2\2\2\u00ea\u00eb\5\31\r\2\u00eb\u00ec\5\7\4\2\u00ec"+
		"\u00ed\5\33\16\2\u00ed\u00ee\5\17\b\2\u00eeF\3\2\2\2\u00ef\u00f0\5#\22"+
		"\2\u00f0\u00f1\5\5\3\2\u00f1\u00f2\5\r\7\2\u00f2\u00f3\5\27\f\2\u00f3"+
		"\u00f4\5\17\b\2\u00f4H\3\2\2\2\u00f5\u00f6\5%\23\2\u00f6\u00f7\5\'\24"+
		"\2\u00f7\u00f8\5\t\5\2\u00f8\u00f9\5\r\7\2\u00f9\u00fa\5\17\b\2\u00fa"+
		"J\3\2\2\2\u00fb\u00fc\5\13\6\2\u00fc\u00fd\5\17\b\2\u00fd\u00fe\5\35\17"+
		"\2\u00fe\u00ff\5\t\5\2\u00ff\u0100\5\25\13\2\u0100L\3\2\2\2\u0101\u0102"+
		"\5\17\b\2\u0102\u0103\5\25\13\2\u0103\u0104\5\37\20\2\u0104N\3\2\2\2\u0105"+
		"\u0106\5\t\5\2\u0106\u0107\5\27\f\2\u0107P\3\2\2\2\u0108\u0109\5\27\f"+
		"\2\u0109\u010a\5)\25\2\u010a\u010b\5\t\5\2\u010b\u010c\5+\26\2\u010cR"+
		"\3\2\2\2\u010d\u010e\7<\2\2\u010e\u010f\7?\2\2\u010fT\3\2\2\2\u0110\u0111"+
		"\7.\2\2\u0111V\3\2\2\2\u0112\u0113\7=\2\2\u0113X\3\2\2\2\u0114\u0115\7"+
		"<\2\2\u0115Z\3\2\2\2\u0116\u0117\7*\2\2\u0117\\\3\2\2\2\u0118\u0119\7"+
		"+\2\2\u0119^\3\2\2\2\u011a\u011b\7-\2\2\u011b`\3\2\2\2\u011c\u011d\7/"+
		"\2\2\u011db\3\2\2\2\u011e\u011f\7\u0080\2\2\u011fd\3\2\2\2\u0120\u0121"+
		"\7~\2\2\u0121f\3\2\2\2\u0122\u0123\7(\2\2\u0123h\3\2\2\2\u0124\u0125\7"+
		"?\2\2\u0125j\3\2\2\2\u0126\u0127\7#\2\2\u0127\u0128\7?\2\2\u0128l\3\2"+
		"\2\2\u0129\u012a\7>\2\2\u012an\3\2\2\2\u012b\u012c\7>\2\2\u012c\u012d"+
		"\7?\2\2\u012dp\3\2\2\2\u012e\u012f\7@\2\2\u012fr\3\2\2\2\u0130\u0131\7"+
		"@\2\2\u0131\u0132\7?\2\2\u0132t\3\2\2\2\u0133\u0139\5-\27\2\u0134\u0138"+
		"\5-\27\2\u0135\u0138\5/\30\2\u0136\u0138\5\61\31\2\u0137\u0134\3\2\2\2"+
		"\u0137\u0135\3\2\2\2\u0137\u0136\3\2\2\2\u0138\u013b\3\2\2\2\u0139\u0137"+
		"\3\2\2\2\u0139\u013a\3\2\2\2\u013av\3\2\2\2\u013b\u0139\3\2\2\2\u013c"+
		"\u013e\7/\2\2\u013d\u013c\3\2\2\2\u013e\u0141\3\2\2\2\u013f\u013d\3\2"+
		"\2\2\u013f\u0140\3\2\2\2\u0140\u0142\3\2\2\2\u0141\u013f\3\2\2\2\u0142"+
		"\u0146\t\31\2\2\u0143\u0145\t\30\2\2\u0144\u0143\3\2\2\2\u0145\u0148\3"+
		"\2\2\2\u0146\u0144\3\2\2\2\u0146\u0147\3\2\2\2\u0147\u014b\3\2\2\2\u0148"+
		"\u0146\3\2\2\2\u0149\u014b\7\62\2\2\u014a\u013f\3\2\2\2\u014a\u0149\3"+
		"\2\2\2\u014bx\3\2\2\2\u014c\u014d\7\61\2\2\u014d\u014e\7\61\2\2\u014e"+
		"\u0152\3\2\2\2\u014f\u0151\n\32\2\2\u0150\u014f\3\2\2\2\u0151\u0154\3"+
		"\2\2\2\u0152\u0150\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0155\3\2\2\2\u0154"+
		"\u0152\3\2\2\2\u0155\u0156\b=\2\2\u0156z\3\2\2\2\u0157\u0158\7\61\2\2"+
		"\u0158\u0159\7,\2\2\u0159\u015e\3\2\2\2\u015a\u015d\5{>\2\u015b\u015d"+
		"\13\2\2\2\u015c\u015a\3\2\2\2\u015c\u015b\3\2\2\2\u015d\u0160\3\2\2\2"+
		"\u015e\u015f\3\2\2\2\u015e\u015c\3\2\2\2\u015f\u0161\3\2\2\2\u0160\u015e"+
		"\3\2\2\2\u0161\u0162\7,\2\2\u0162\u0163\7\61\2\2\u0163\u0164\3\2\2\2\u0164"+
		"\u0165\b>\2\2\u0165|\3\2\2\2\u0166\u0168\t\33\2\2\u0167\u0166\3\2\2\2"+
		"\u0168\u0169\3\2\2\2\u0169\u0167\3\2\2\2\u0169\u016a\3\2\2\2\u016a\u016b"+
		"\3\2\2\2\u016b\u016c\b?\2\2\u016c~\3\2\2\2\f\2\u0137\u0139\u013f\u0146"+
		"\u014a\u0152\u015c\u015e\u0169\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}