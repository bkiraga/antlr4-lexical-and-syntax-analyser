// Generated from cal.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class calParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		VARIABLE=1, CONSTANT=2, RETURN=3, INTEGER=4, BOOLEAN=5, VOID=6, MAIN=7, 
		IF=8, ELSE=9, TRUE=10, FALSE=11, WHILE=12, BEGIN=13, END=14, IS=15, SKIP_=16, 
		ASSIGN=17, COMMA=18, SEMI_COLON=19, COLON=20, LBR=21, RBR=22, PLUS=23, 
		MINUS=24, NEG=25, OR=26, AND=27, EQUAL=28, NOT_EQUAL=29, LESS_THAN=30, 
		LESS_THAN_EQ=31, MORE_THAN=32, MORE_THAN_EQ=33, IDENTIFIER=34, NUMBER=35, 
		COMMENT=36, BLOCKCOMMENT=37, WS=38;
	public static final int
		RULE_program = 0, RULE_decl_list = 1, RULE_decl = 2, RULE_var_decl = 3, 
		RULE_const_decl = 4, RULE_function_list = 5, RULE_function = 6, RULE_type = 7, 
		RULE_parameter_list = 8, RULE_nemp_parameter_list = 9, RULE_main = 10, 
		RULE_statement_block = 11, RULE_statement = 12, RULE_expression = 13, 
		RULE_binary_arth_op = 14, RULE_fragment_ = 15, RULE_condition = 16, RULE_comp_op = 17, 
		RULE_arg_list = 18, RULE_nemp_arg_list = 19;
	public static final String[] ruleNames = {
		"program", "decl_list", "decl", "var_decl", "const_decl", "function_list", 
		"function", "type", "parameter_list", "nemp_parameter_list", "main", "statement_block", 
		"statement", "expression", "binary_arth_op", "fragment_", "condition", 
		"comp_op", "arg_list", "nemp_arg_list"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "':='", "','", "';'", "':'", "'('", "')'", 
		"'+'", "'-'", "'~'", "'|'", "'&'", "'='", "'!='", "'<'", "'<='", "'>'", 
		"'>='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "VARIABLE", "CONSTANT", "RETURN", "INTEGER", "BOOLEAN", "VOID", 
		"MAIN", "IF", "ELSE", "TRUE", "FALSE", "WHILE", "BEGIN", "END", "IS", 
		"SKIP_", "ASSIGN", "COMMA", "SEMI_COLON", "COLON", "LBR", "RBR", "PLUS", 
		"MINUS", "NEG", "OR", "AND", "EQUAL", "NOT_EQUAL", "LESS_THAN", "LESS_THAN_EQ", 
		"MORE_THAN", "MORE_THAN_EQ", "IDENTIFIER", "NUMBER", "COMMENT", "BLOCKCOMMENT", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "cal.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public calParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public Decl_listContext decl_list() {
			return getRuleContext(Decl_listContext.class,0);
		}
		public Function_listContext function_list() {
			return getRuleContext(Function_listContext.class,0);
		}
		public MainContext main() {
			return getRuleContext(MainContext.class,0);
		}
		public TerminalNode EOF() { return getToken(calParser.EOF, 0); }
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			decl_list();
			setState(41);
			function_list();
			setState(42);
			main();
			setState(43);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Decl_listContext extends ParserRuleContext {
		public DeclContext decl() {
			return getRuleContext(DeclContext.class,0);
		}
		public TerminalNode SEMI_COLON() { return getToken(calParser.SEMI_COLON, 0); }
		public Decl_listContext decl_list() {
			return getRuleContext(Decl_listContext.class,0);
		}
		public Decl_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decl_list; }
	}

	public final Decl_listContext decl_list() throws RecognitionException {
		Decl_listContext _localctx = new Decl_listContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_decl_list);
		try {
			setState(50);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VARIABLE:
			case CONSTANT:
				enterOuterAlt(_localctx, 1);
				{
				setState(45);
				decl();
				setState(46);
				match(SEMI_COLON);
				setState(47);
				decl_list();
				}
				break;
			case INTEGER:
			case BOOLEAN:
			case VOID:
			case MAIN:
			case IF:
			case ELSE:
			case WHILE:
			case BEGIN:
			case END:
			case SKIP_:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclContext extends ParserRuleContext {
		public Var_declContext var_decl() {
			return getRuleContext(Var_declContext.class,0);
		}
		public Const_declContext const_decl() {
			return getRuleContext(Const_declContext.class,0);
		}
		public DeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decl; }
	}

	public final DeclContext decl() throws RecognitionException {
		DeclContext _localctx = new DeclContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_decl);
		try {
			setState(54);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VARIABLE:
				enterOuterAlt(_localctx, 1);
				{
				setState(52);
				var_decl();
				}
				break;
			case CONSTANT:
				enterOuterAlt(_localctx, 2);
				{
				setState(53);
				const_decl();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_declContext extends ParserRuleContext {
		public TerminalNode VARIABLE() { return getToken(calParser.VARIABLE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(calParser.IDENTIFIER, 0); }
		public TerminalNode COLON() { return getToken(calParser.COLON, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Var_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_decl; }
	}

	public final Var_declContext var_decl() throws RecognitionException {
		Var_declContext _localctx = new Var_declContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_var_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			match(VARIABLE);
			setState(57);
			match(IDENTIFIER);
			setState(58);
			match(COLON);
			setState(59);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Const_declContext extends ParserRuleContext {
		public TerminalNode CONSTANT() { return getToken(calParser.CONSTANT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(calParser.IDENTIFIER, 0); }
		public TerminalNode COLON() { return getToken(calParser.COLON, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(calParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Const_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_const_decl; }
	}

	public final Const_declContext const_decl() throws RecognitionException {
		Const_declContext _localctx = new Const_declContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_const_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			match(CONSTANT);
			setState(62);
			match(IDENTIFIER);
			setState(63);
			match(COLON);
			setState(64);
			type();
			setState(65);
			match(ASSIGN);
			setState(66);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_listContext extends ParserRuleContext {
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public Function_listContext function_list() {
			return getRuleContext(Function_listContext.class,0);
		}
		public Function_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_list; }
	}

	public final Function_listContext function_list() throws RecognitionException {
		Function_listContext _localctx = new Function_listContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_function_list);
		try {
			setState(72);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER:
			case BOOLEAN:
			case VOID:
				enterOuterAlt(_localctx, 1);
				{
				setState(68);
				function();
				setState(69);
				function_list();
				}
				break;
			case MAIN:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(calParser.IDENTIFIER, 0); }
		public List<TerminalNode> LBR() { return getTokens(calParser.LBR); }
		public TerminalNode LBR(int i) {
			return getToken(calParser.LBR, i);
		}
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public List<TerminalNode> RBR() { return getTokens(calParser.RBR); }
		public TerminalNode RBR(int i) {
			return getToken(calParser.RBR, i);
		}
		public TerminalNode IS() { return getToken(calParser.IS, 0); }
		public Decl_listContext decl_list() {
			return getRuleContext(Decl_listContext.class,0);
		}
		public TerminalNode BEGIN() { return getToken(calParser.BEGIN, 0); }
		public Statement_blockContext statement_block() {
			return getRuleContext(Statement_blockContext.class,0);
		}
		public TerminalNode RETURN() { return getToken(calParser.RETURN, 0); }
		public TerminalNode SEMI_COLON() { return getToken(calParser.SEMI_COLON, 0); }
		public TerminalNode END() { return getToken(calParser.END, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			type();
			setState(75);
			match(IDENTIFIER);
			setState(76);
			match(LBR);
			setState(77);
			parameter_list();
			setState(78);
			match(RBR);
			setState(79);
			match(IS);
			setState(80);
			decl_list();
			setState(81);
			match(BEGIN);
			setState(82);
			statement_block();
			setState(83);
			match(RETURN);
			setState(84);
			match(LBR);
			setState(87);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUE:
			case FALSE:
			case LBR:
			case MINUS:
			case IDENTIFIER:
			case NUMBER:
				{
				setState(85);
				expression();
				}
				break;
			case RBR:
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(89);
			match(RBR);
			setState(90);
			match(SEMI_COLON);
			setState(91);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(calParser.INTEGER, 0); }
		public TerminalNode BOOLEAN() { return getToken(calParser.BOOLEAN, 0); }
		public TerminalNode VOID() { return getToken(calParser.VOID, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTEGER) | (1L << BOOLEAN) | (1L << VOID))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_listContext extends ParserRuleContext {
		public Nemp_parameter_listContext nemp_parameter_list() {
			return getRuleContext(Nemp_parameter_listContext.class,0);
		}
		public Parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_list; }
	}

	public final Parameter_listContext parameter_list() throws RecognitionException {
		Parameter_listContext _localctx = new Parameter_listContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_parameter_list);
		try {
			setState(97);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(95);
				nemp_parameter_list();
				}
				break;
			case RBR:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Nemp_parameter_listContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(calParser.IDENTIFIER, 0); }
		public TerminalNode COLON() { return getToken(calParser.COLON, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(calParser.COMMA, 0); }
		public Nemp_parameter_listContext nemp_parameter_list() {
			return getRuleContext(Nemp_parameter_listContext.class,0);
		}
		public Nemp_parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nemp_parameter_list; }
	}

	public final Nemp_parameter_listContext nemp_parameter_list() throws RecognitionException {
		Nemp_parameter_listContext _localctx = new Nemp_parameter_listContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_nemp_parameter_list);
		try {
			setState(108);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(99);
				match(IDENTIFIER);
				setState(100);
				match(COLON);
				setState(101);
				type();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(102);
				match(IDENTIFIER);
				setState(103);
				match(COLON);
				setState(104);
				type();
				setState(105);
				match(COMMA);
				setState(106);
				nemp_parameter_list();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainContext extends ParserRuleContext {
		public TerminalNode MAIN() { return getToken(calParser.MAIN, 0); }
		public TerminalNode BEGIN() { return getToken(calParser.BEGIN, 0); }
		public Decl_listContext decl_list() {
			return getRuleContext(Decl_listContext.class,0);
		}
		public Statement_blockContext statement_block() {
			return getRuleContext(Statement_blockContext.class,0);
		}
		public TerminalNode END() { return getToken(calParser.END, 0); }
		public MainContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_main; }
	}

	public final MainContext main() throws RecognitionException {
		MainContext _localctx = new MainContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_main);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			match(MAIN);
			setState(111);
			match(BEGIN);
			setState(112);
			decl_list();
			setState(113);
			statement_block();
			setState(114);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Statement_blockContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public Statement_blockContext statement_block() {
			return getRuleContext(Statement_blockContext.class,0);
		}
		public Statement_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement_block; }
	}

	public final Statement_blockContext statement_block() throws RecognitionException {
		Statement_blockContext _localctx = new Statement_blockContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_statement_block);
		try {
			setState(120);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IF:
			case ELSE:
			case WHILE:
			case BEGIN:
			case SKIP_:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(116);
				statement();
				setState(117);
				statement_block();
				}
				}
				break;
			case RETURN:
			case END:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(calParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(calParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SEMI_COLON() { return getToken(calParser.SEMI_COLON, 0); }
		public TerminalNode LBR() { return getToken(calParser.LBR, 0); }
		public Arg_listContext arg_list() {
			return getRuleContext(Arg_listContext.class,0);
		}
		public TerminalNode RBR() { return getToken(calParser.RBR, 0); }
		public TerminalNode BEGIN() { return getToken(calParser.BEGIN, 0); }
		public Statement_blockContext statement_block() {
			return getRuleContext(Statement_blockContext.class,0);
		}
		public TerminalNode END() { return getToken(calParser.END, 0); }
		public TerminalNode IF() { return getToken(calParser.IF, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public TerminalNode ELSE() { return getToken(calParser.ELSE, 0); }
		public TerminalNode WHILE() { return getToken(calParser.WHILE, 0); }
		public TerminalNode SKIP_() { return getToken(calParser.SKIP_, 0); }
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_statement);
		try {
			setState(156);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(122);
				match(IDENTIFIER);
				setState(123);
				match(ASSIGN);
				setState(124);
				expression();
				setState(125);
				match(SEMI_COLON);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(127);
				match(IDENTIFIER);
				setState(128);
				match(LBR);
				setState(129);
				arg_list();
				setState(130);
				match(RBR);
				setState(131);
				match(SEMI_COLON);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(133);
				match(BEGIN);
				setState(134);
				statement_block();
				setState(135);
				match(END);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(137);
				match(IF);
				setState(138);
				condition(0);
				setState(139);
				match(BEGIN);
				setState(140);
				statement_block();
				setState(141);
				match(END);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(143);
				match(ELSE);
				setState(144);
				match(BEGIN);
				setState(145);
				statement_block();
				setState(146);
				match(END);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(148);
				match(WHILE);
				setState(149);
				condition(0);
				setState(150);
				match(BEGIN);
				setState(151);
				statement_block();
				setState(152);
				match(END);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(154);
				match(SKIP_);
				setState(155);
				match(SEMI_COLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public List<Fragment_Context> fragment_() {
			return getRuleContexts(Fragment_Context.class);
		}
		public Fragment_Context fragment_(int i) {
			return getRuleContext(Fragment_Context.class,i);
		}
		public Binary_arth_opContext binary_arth_op() {
			return getRuleContext(Binary_arth_opContext.class,0);
		}
		public TerminalNode LBR() { return getToken(calParser.LBR, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RBR() { return getToken(calParser.RBR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(calParser.IDENTIFIER, 0); }
		public Arg_listContext arg_list() {
			return getRuleContext(Arg_listContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_expression);
		try {
			setState(172);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(158);
				fragment_(0);
				setState(159);
				binary_arth_op();
				setState(160);
				fragment_(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(162);
				match(LBR);
				setState(163);
				expression();
				setState(164);
				match(RBR);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(166);
				match(IDENTIFIER);
				setState(167);
				match(LBR);
				setState(168);
				arg_list();
				setState(169);
				match(RBR);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(171);
				fragment_(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Binary_arth_opContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(calParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(calParser.MINUS, 0); }
		public Binary_arth_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binary_arth_op; }
	}

	public final Binary_arth_opContext binary_arth_op() throws RecognitionException {
		Binary_arth_opContext _localctx = new Binary_arth_opContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_binary_arth_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			_la = _input.LA(1);
			if ( !(_la==PLUS || _la==MINUS) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fragment_Context extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(calParser.IDENTIFIER, 0); }
		public TerminalNode MINUS() { return getToken(calParser.MINUS, 0); }
		public TerminalNode NUMBER() { return getToken(calParser.NUMBER, 0); }
		public TerminalNode TRUE() { return getToken(calParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(calParser.FALSE, 0); }
		public TerminalNode LBR() { return getToken(calParser.LBR, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RBR() { return getToken(calParser.RBR, 0); }
		public Arg_listContext arg_list() {
			return getRuleContext(Arg_listContext.class,0);
		}
		public List<Fragment_Context> fragment_() {
			return getRuleContexts(Fragment_Context.class);
		}
		public Fragment_Context fragment_(int i) {
			return getRuleContext(Fragment_Context.class,i);
		}
		public Binary_arth_opContext binary_arth_op() {
			return getRuleContext(Binary_arth_opContext.class,0);
		}
		public Fragment_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fragment_; }
	}

	public final Fragment_Context fragment_() throws RecognitionException {
		return fragment_(0);
	}

	private Fragment_Context fragment_(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Fragment_Context _localctx = new Fragment_Context(_ctx, _parentState);
		Fragment_Context _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_fragment_, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(177);
				match(IDENTIFIER);
				}
				break;
			case 2:
				{
				setState(178);
				match(MINUS);
				setState(179);
				match(IDENTIFIER);
				}
				break;
			case 3:
				{
				setState(180);
				match(NUMBER);
				}
				break;
			case 4:
				{
				setState(181);
				match(TRUE);
				}
				break;
			case 5:
				{
				setState(182);
				match(FALSE);
				}
				break;
			case 6:
				{
				setState(183);
				match(LBR);
				setState(184);
				expression();
				setState(185);
				match(RBR);
				}
				break;
			case 7:
				{
				setState(187);
				match(IDENTIFIER);
				setState(188);
				match(LBR);
				setState(189);
				arg_list();
				setState(190);
				match(RBR);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(200);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Fragment_Context(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_fragment_);
					setState(194);
					if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
					setState(195);
					binary_arth_op();
					setState(196);
					fragment_(4);
					}
					} 
				}
				setState(202);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public TerminalNode NEG() { return getToken(calParser.NEG, 0); }
		public List<ConditionContext> condition() {
			return getRuleContexts(ConditionContext.class);
		}
		public ConditionContext condition(int i) {
			return getRuleContext(ConditionContext.class,i);
		}
		public TerminalNode LBR() { return getToken(calParser.LBR, 0); }
		public TerminalNode RBR() { return getToken(calParser.RBR, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Comp_opContext comp_op() {
			return getRuleContext(Comp_opContext.class,0);
		}
		public TerminalNode OR() { return getToken(calParser.OR, 0); }
		public TerminalNode AND() { return getToken(calParser.AND, 0); }
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
	}

	public final ConditionContext condition() throws RecognitionException {
		return condition(0);
	}

	private ConditionContext condition(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ConditionContext _localctx = new ConditionContext(_ctx, _parentState);
		ConditionContext _prevctx = _localctx;
		int _startState = 32;
		enterRecursionRule(_localctx, 32, RULE_condition, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(214);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(204);
				match(NEG);
				setState(205);
				condition(5);
				}
				break;
			case 2:
				{
				setState(206);
				match(LBR);
				setState(207);
				condition(0);
				setState(208);
				match(RBR);
				}
				break;
			case 3:
				{
				setState(210);
				expression();
				setState(211);
				comp_op();
				setState(212);
				expression();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(224);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(222);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						_localctx = new ConditionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condition);
						setState(216);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(217);
						match(OR);
						setState(218);
						condition(3);
						}
						break;
					case 2:
						{
						_localctx = new ConditionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condition);
						setState(219);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(220);
						match(AND);
						setState(221);
						condition(2);
						}
						break;
					}
					} 
				}
				setState(226);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Comp_opContext extends ParserRuleContext {
		public TerminalNode EQUAL() { return getToken(calParser.EQUAL, 0); }
		public TerminalNode NOT_EQUAL() { return getToken(calParser.NOT_EQUAL, 0); }
		public TerminalNode LESS_THAN() { return getToken(calParser.LESS_THAN, 0); }
		public TerminalNode LESS_THAN_EQ() { return getToken(calParser.LESS_THAN_EQ, 0); }
		public TerminalNode MORE_THAN() { return getToken(calParser.MORE_THAN, 0); }
		public TerminalNode MORE_THAN_EQ() { return getToken(calParser.MORE_THAN_EQ, 0); }
		public Comp_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comp_op; }
	}

	public final Comp_opContext comp_op() throws RecognitionException {
		Comp_opContext _localctx = new Comp_opContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_comp_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(227);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << NOT_EQUAL) | (1L << LESS_THAN) | (1L << LESS_THAN_EQ) | (1L << MORE_THAN) | (1L << MORE_THAN_EQ))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Arg_listContext extends ParserRuleContext {
		public Nemp_arg_listContext nemp_arg_list() {
			return getRuleContext(Nemp_arg_listContext.class,0);
		}
		public Arg_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arg_list; }
	}

	public final Arg_listContext arg_list() throws RecognitionException {
		Arg_listContext _localctx = new Arg_listContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_arg_list);
		try {
			setState(231);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(229);
				nemp_arg_list();
				}
				break;
			case RBR:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Nemp_arg_listContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(calParser.IDENTIFIER, 0); }
		public TerminalNode COMMA() { return getToken(calParser.COMMA, 0); }
		public Nemp_arg_listContext nemp_arg_list() {
			return getRuleContext(Nemp_arg_listContext.class,0);
		}
		public Nemp_arg_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nemp_arg_list; }
	}

	public final Nemp_arg_listContext nemp_arg_list() throws RecognitionException {
		Nemp_arg_listContext _localctx = new Nemp_arg_listContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_nemp_arg_list);
		try {
			setState(237);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(233);
				match(IDENTIFIER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(234);
				match(IDENTIFIER);
				setState(235);
				match(COMMA);
				setState(236);
				nemp_arg_list();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 15:
			return fragment__sempred((Fragment_Context)_localctx, predIndex);
		case 16:
			return condition_sempred((ConditionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean fragment__sempred(Fragment_Context _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 3);
		}
		return true;
	}
	private boolean condition_sempred(ConditionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 2);
		case 2:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3(\u00f2\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3"+
		"\5\3\65\n\3\3\4\3\4\5\49\n\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\7\3\7\3\7\3\7\5\7K\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\5\bZ\n\b\3\b\3\b\3\b\3\b\3\t\3\t\3\n\3\n\5\nd\n\n\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13o\n\13\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\r\3\r\3\r\3\r\5\r{\n\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u009f"+
		"\n\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\5\17\u00af\n\17\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u00c3\n\21\3\21\3\21\3\21"+
		"\3\21\7\21\u00c9\n\21\f\21\16\21\u00cc\13\21\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u00d9\n\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\7\22\u00e1\n\22\f\22\16\22\u00e4\13\22\3\23\3\23\3\24\3\24\5\24"+
		"\u00ea\n\24\3\25\3\25\3\25\3\25\5\25\u00f0\n\25\3\25\2\4 \"\26\2\4\6\b"+
		"\n\f\16\20\22\24\26\30\32\34\36 \"$&(\2\5\3\2\6\b\3\2\31\32\3\2\36#\2"+
		"\u00fa\2*\3\2\2\2\4\64\3\2\2\2\68\3\2\2\2\b:\3\2\2\2\n?\3\2\2\2\fJ\3\2"+
		"\2\2\16L\3\2\2\2\20_\3\2\2\2\22c\3\2\2\2\24n\3\2\2\2\26p\3\2\2\2\30z\3"+
		"\2\2\2\32\u009e\3\2\2\2\34\u00ae\3\2\2\2\36\u00b0\3\2\2\2 \u00c2\3\2\2"+
		"\2\"\u00d8\3\2\2\2$\u00e5\3\2\2\2&\u00e9\3\2\2\2(\u00ef\3\2\2\2*+\5\4"+
		"\3\2+,\5\f\7\2,-\5\26\f\2-.\7\2\2\3.\3\3\2\2\2/\60\5\6\4\2\60\61\7\25"+
		"\2\2\61\62\5\4\3\2\62\65\3\2\2\2\63\65\3\2\2\2\64/\3\2\2\2\64\63\3\2\2"+
		"\2\65\5\3\2\2\2\669\5\b\5\2\679\5\n\6\28\66\3\2\2\28\67\3\2\2\29\7\3\2"+
		"\2\2:;\7\3\2\2;<\7$\2\2<=\7\26\2\2=>\5\20\t\2>\t\3\2\2\2?@\7\4\2\2@A\7"+
		"$\2\2AB\7\26\2\2BC\5\20\t\2CD\7\23\2\2DE\5\34\17\2E\13\3\2\2\2FG\5\16"+
		"\b\2GH\5\f\7\2HK\3\2\2\2IK\3\2\2\2JF\3\2\2\2JI\3\2\2\2K\r\3\2\2\2LM\5"+
		"\20\t\2MN\7$\2\2NO\7\27\2\2OP\5\22\n\2PQ\7\30\2\2QR\7\21\2\2RS\5\4\3\2"+
		"ST\7\17\2\2TU\5\30\r\2UV\7\5\2\2VY\7\27\2\2WZ\5\34\17\2XZ\3\2\2\2YW\3"+
		"\2\2\2YX\3\2\2\2Z[\3\2\2\2[\\\7\30\2\2\\]\7\25\2\2]^\7\20\2\2^\17\3\2"+
		"\2\2_`\t\2\2\2`\21\3\2\2\2ad\5\24\13\2bd\3\2\2\2ca\3\2\2\2cb\3\2\2\2d"+
		"\23\3\2\2\2ef\7$\2\2fg\7\26\2\2go\5\20\t\2hi\7$\2\2ij\7\26\2\2jk\5\20"+
		"\t\2kl\7\24\2\2lm\5\24\13\2mo\3\2\2\2ne\3\2\2\2nh\3\2\2\2o\25\3\2\2\2"+
		"pq\7\t\2\2qr\7\17\2\2rs\5\4\3\2st\5\30\r\2tu\7\20\2\2u\27\3\2\2\2vw\5"+
		"\32\16\2wx\5\30\r\2x{\3\2\2\2y{\3\2\2\2zv\3\2\2\2zy\3\2\2\2{\31\3\2\2"+
		"\2|}\7$\2\2}~\7\23\2\2~\177\5\34\17\2\177\u0080\7\25\2\2\u0080\u009f\3"+
		"\2\2\2\u0081\u0082\7$\2\2\u0082\u0083\7\27\2\2\u0083\u0084\5&\24\2\u0084"+
		"\u0085\7\30\2\2\u0085\u0086\7\25\2\2\u0086\u009f\3\2\2\2\u0087\u0088\7"+
		"\17\2\2\u0088\u0089\5\30\r\2\u0089\u008a\7\20\2\2\u008a\u009f\3\2\2\2"+
		"\u008b\u008c\7\n\2\2\u008c\u008d\5\"\22\2\u008d\u008e\7\17\2\2\u008e\u008f"+
		"\5\30\r\2\u008f\u0090\7\20\2\2\u0090\u009f\3\2\2\2\u0091\u0092\7\13\2"+
		"\2\u0092\u0093\7\17\2\2\u0093\u0094\5\30\r\2\u0094\u0095\7\20\2\2\u0095"+
		"\u009f\3\2\2\2\u0096\u0097\7\16\2\2\u0097\u0098\5\"\22\2\u0098\u0099\7"+
		"\17\2\2\u0099\u009a\5\30\r\2\u009a\u009b\7\20\2\2\u009b\u009f\3\2\2\2"+
		"\u009c\u009d\7\22\2\2\u009d\u009f\7\25\2\2\u009e|\3\2\2\2\u009e\u0081"+
		"\3\2\2\2\u009e\u0087\3\2\2\2\u009e\u008b\3\2\2\2\u009e\u0091\3\2\2\2\u009e"+
		"\u0096\3\2\2\2\u009e\u009c\3\2\2\2\u009f\33\3\2\2\2\u00a0\u00a1\5 \21"+
		"\2\u00a1\u00a2\5\36\20\2\u00a2\u00a3\5 \21\2\u00a3\u00af\3\2\2\2\u00a4"+
		"\u00a5\7\27\2\2\u00a5\u00a6\5\34\17\2\u00a6\u00a7\7\30\2\2\u00a7\u00af"+
		"\3\2\2\2\u00a8\u00a9\7$\2\2\u00a9\u00aa\7\27\2\2\u00aa\u00ab\5&\24\2\u00ab"+
		"\u00ac\7\30\2\2\u00ac\u00af\3\2\2\2\u00ad\u00af\5 \21\2\u00ae\u00a0\3"+
		"\2\2\2\u00ae\u00a4\3\2\2\2\u00ae\u00a8\3\2\2\2\u00ae\u00ad\3\2\2\2\u00af"+
		"\35\3\2\2\2\u00b0\u00b1\t\3\2\2\u00b1\37\3\2\2\2\u00b2\u00b3\b\21\1\2"+
		"\u00b3\u00c3\7$\2\2\u00b4\u00b5\7\32\2\2\u00b5\u00c3\7$\2\2\u00b6\u00c3"+
		"\7%\2\2\u00b7\u00c3\7\f\2\2\u00b8\u00c3\7\r\2\2\u00b9\u00ba\7\27\2\2\u00ba"+
		"\u00bb\5\34\17\2\u00bb\u00bc\7\30\2\2\u00bc\u00c3\3\2\2\2\u00bd\u00be"+
		"\7$\2\2\u00be\u00bf\7\27\2\2\u00bf\u00c0\5&\24\2\u00c0\u00c1\7\30\2\2"+
		"\u00c1\u00c3\3\2\2\2\u00c2\u00b2\3\2\2\2\u00c2\u00b4\3\2\2\2\u00c2\u00b6"+
		"\3\2\2\2\u00c2\u00b7\3\2\2\2\u00c2\u00b8\3\2\2\2\u00c2\u00b9\3\2\2\2\u00c2"+
		"\u00bd\3\2\2\2\u00c3\u00ca\3\2\2\2\u00c4\u00c5\f\5\2\2\u00c5\u00c6\5\36"+
		"\20\2\u00c6\u00c7\5 \21\6\u00c7\u00c9\3\2\2\2\u00c8\u00c4\3\2\2\2\u00c9"+
		"\u00cc\3\2\2\2\u00ca\u00c8\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb!\3\2\2\2"+
		"\u00cc\u00ca\3\2\2\2\u00cd\u00ce\b\22\1\2\u00ce\u00cf\7\33\2\2\u00cf\u00d9"+
		"\5\"\22\7\u00d0\u00d1\7\27\2\2\u00d1\u00d2\5\"\22\2\u00d2\u00d3\7\30\2"+
		"\2\u00d3\u00d9\3\2\2\2\u00d4\u00d5\5\34\17\2\u00d5\u00d6\5$\23\2\u00d6"+
		"\u00d7\5\34\17\2\u00d7\u00d9\3\2\2\2\u00d8\u00cd\3\2\2\2\u00d8\u00d0\3"+
		"\2\2\2\u00d8\u00d4\3\2\2\2\u00d9\u00e2\3\2\2\2\u00da\u00db\f\4\2\2\u00db"+
		"\u00dc\7\34\2\2\u00dc\u00e1\5\"\22\5\u00dd\u00de\f\3\2\2\u00de\u00df\7"+
		"\35\2\2\u00df\u00e1\5\"\22\4\u00e0\u00da\3\2\2\2\u00e0\u00dd\3\2\2\2\u00e1"+
		"\u00e4\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e2\u00e3\3\2\2\2\u00e3#\3\2\2\2"+
		"\u00e4\u00e2\3\2\2\2\u00e5\u00e6\t\4\2\2\u00e6%\3\2\2\2\u00e7\u00ea\5"+
		"(\25\2\u00e8\u00ea\3\2\2\2\u00e9\u00e7\3\2\2\2\u00e9\u00e8\3\2\2\2\u00ea"+
		"\'\3\2\2\2\u00eb\u00f0\7$\2\2\u00ec\u00ed\7$\2\2\u00ed\u00ee\7\24\2\2"+
		"\u00ee\u00f0\5(\25\2\u00ef\u00eb\3\2\2\2\u00ef\u00ec\3\2\2\2\u00f0)\3"+
		"\2\2\2\22\648JYcnz\u009e\u00ae\u00c2\u00ca\u00d8\u00e0\u00e2\u00e9\u00ef";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}