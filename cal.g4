grammar cal;

program:                decl_list function_list main EOF;

decl_list:              decl SEMI_COLON decl_list
                        |
                        ;

decl:                   var_decl | const_decl;

var_decl:               VARIABLE IDENTIFIER COLON type;

const_decl:             CONSTANT IDENTIFIER COLON type ASSIGN expression;

function_list:          function function_list
                        |
                        ;

function:               type IDENTIFIER LBR parameter_list RBR IS
                        decl_list
                        BEGIN
                        statement_block
                        RETURN LBR (expression | ) RBR SEMI_COLON
                        END
                        ;

type:                   INTEGER | BOOLEAN | VOID;

parameter_list:         nemp_parameter_list
                        |
                        ;

nemp_parameter_list:    IDENTIFIER COLON type
                        | IDENTIFIER COLON type COMMA nemp_parameter_list
                        ;

main:                   MAIN
                        BEGIN
                        decl_list
                        statement_block
                        END
                        ;

statement_block:        (statement statement_block)
                        |
                        ;

statement:              IDENTIFIER ASSIGN expression SEMI_COLON
                        | IDENTIFIER LBR arg_list RBR SEMI_COLON
                        | BEGIN statement_block END
                        | IF condition BEGIN statement_block END
                        | ELSE BEGIN statement_block END
                        | WHILE condition BEGIN statement_block END
                        | SKIP_ SEMI_COLON
                        ;

expression:             fragment_ binary_arth_op fragment_
                        | LBR expression RBR
                        | IDENTIFIER LBR arg_list RBR
                        | fragment_
                        ;

binary_arth_op:         PLUS | MINUS;

fragment_:              IDENTIFIER
                        | MINUS IDENTIFIER
                        | NUMBER
                        | TRUE
                        | FALSE
                        | fragment_ binary_arth_op fragment_
                        | LBR expression RBR
                        | IDENTIFIER LBR arg_list RBR
                        ;

condition:              NEG condition
                        | LBR condition RBR
                        | expression comp_op expression
                        | condition OR condition
                        | condition AND condition
                        ;

comp_op:                EQUAL | NOT_EQUAL | LESS_THAN | LESS_THAN_EQ | MORE_THAN | MORE_THAN_EQ;

arg_list:               nemp_arg_list
                        |
                        ;

nemp_arg_list:          IDENTIFIER
                        | IDENTIFIER COMMA nemp_arg_list
                        ;

fragment V:             'v' | 'V';
fragment A:             'a' | 'A';
fragment R:             'r' | 'R';
fragment I:             'i' | 'I';
fragment B:             'b' | 'B';
fragment L:             'l' | 'L';
fragment E:             'e' | 'E';
fragment C:             'c' | 'C';
fragment O:             'o' | 'O';
fragment N:             'n' | 'N';
fragment S:             's' | 'S';
fragment T:             't' | 'T';
fragment U:             'u' | 'U';
fragment G:             'g' | 'G';
fragment D:             'd' | 'D';
fragment M:             'm' | 'M';
fragment F:             'f' | 'F';
fragment W:             'w' | 'W';
fragment H:             'h' | 'H';
fragment K:             'k' | 'K';
fragment P:             'p' | 'P';

fragment LETTER:        [a-zA-Z];
fragment DIGGIT:        [0-9];
fragment UNDERSCORE:    '_';

VARIABLE:               V A R I A B L E;
CONSTANT:               C O N S T A N T;
RETURN:                 R E T U R N;
INTEGER:                I N T E G E R;
BOOLEAN:                B O O L E A N;
VOID:                   V O I D;
MAIN:                   M A I N;
IF:                     I F;
ELSE:                   E L S E;
TRUE:                   T R U E;
FALSE:                  F A L S E;
WHILE:                  W H I L E;  
BEGIN:                  B E G I N;
END:                    E N D;
IS:                     I S;
SKIP_:                   S K I P;

ASSIGN:                 ':=';
COMMA:                  ',';
SEMI_COLON:             ';';
COLON:                  ':';
LBR:                    '(';
RBR:                    ')';
PLUS:                   '+';
MINUS:                  '-';
NEG:                    '~';
OR:                     '|';
AND:                    '&';
EQUAL:                  '=';
NOT_EQUAL:              '!=';
LESS_THAN:              '<';
LESS_THAN_EQ:           '<=';
MORE_THAN:              '>';
MORE_THAN_EQ:           '>=';

IDENTIFIER:             LETTER(LETTER | DIGGIT | UNDERSCORE)*;
NUMBER:                 ('-')*([1-9][0-9]*)|'0';

COMMENT:                '//' ~[\r\n]* -> skip;
BLOCKCOMMENT:           '/*' (BLOCKCOMMENT|.)*? '*/' -> skip;
WS:			            [ \t\n\r]+ -> skip;